package services;
import dto.User;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import java.security.MessageDigest;
import java.security.MessageDigestSpi;
import java.security.NoSuchAlgorithmException;

import data.Database;


public class UserService {
	
	public void login(HttpServletRequest request, HttpServletResponse response ) {
		String username = request.getParameter("username");//need to escape special characters
		String password = request.getParameter("password");//need to escape special characters
		try {
			this.validateLogin(username, password);
			HttpSession session = request.getSession(true);
			User user = this.authenticateLogin(username, password);
			
			session.setAttribute("user", user);
			request.setAttribute("result", "Logged successfully !");
		}catch(Exception e) {
			//e.printStackTrace();
			request.setAttribute("result", e.getMessage());
		}
	}
	
	public void logout(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if(session != null) session.invalidate();
	}
	
	public boolean isSessionAuthenticated( HttpServletRequest request, HttpServletResponse response ) {
		
		User sessionUser = (User) request.getSession().getAttribute("user");//need to escape sp) 
		if(sessionUser != null) {
			
			Database database = new Database();
			Hashtable<String, String> databaseUserData = database.getTable("users").where("id", sessionUser.getId()).getAllColumns();
			if(databaseUserData != null) {
				User databaseUser = new User(databaseUserData);
				if(sessionUser.getPassword().equals(databaseUser.getPassword())) {
					return true;
				}
			}
			
		}
		return false;
	}
	
	public User authenticateLogin(String username, String password) throws Exception {
		Database database = new Database();
		Hashtable<String, String> userData = database.getTable("users").where("username", username).getAllColumns();
		if(userData != null) {
			User user = new User(userData);
			if(this.comparePasswords(password, user.getPassword())) {
				return user;
			}else {
				throw new Exception("Invalid credentials");
			}
		}else {
			throw new Exception("Invalid credentials");
		}
	}
	
	public void isUserExists(String username) throws Exception {
		Database database = new Database();
		if(database.getTable("users").where("username", username).getColumn("id") != null) {
			throw new Exception("User already exists !");
		}
		
	}
	
	public void signup(HttpServletRequest request, HttpServletResponse response) {
		
		String username = request.getParameter("username");//need to escape special characters
		String password = request.getParameter("password");//need to escape special characters
		String passwordConfirmation = request.getParameter("passwordConfirmation");//need to escape special characters
		
		try {
			
			this.valdiateDataForRegistration(username, password, passwordConfirmation);
			this.isUserExists(username);
			this.insertNewUser(username, password, passwordConfirmation);

			request.setAttribute("result", "Account was successfully created!");
		}catch(Exception e) {
			request.setAttribute("result", e.getMessage());
		}
		
	}
	
	public void changePassword(HttpServletRequest request, HttpServletResponse response) {
		try {
			if(this.isSessionAuthenticated(request, response)) {
				//todo 
				//getData from form, compare old pass with sessioned user pass, generate new hashed password, save to xml
				String oldPasswordInputed = request.getParameter("oldPassword");
				String newPassword = request.getParameter("newPassword");
				String newPasswordConfirmation = request.getParameter("newPasswordConfirmation");
				String oldPasswordStored = ((User) request.getSession().getAttribute("user")).getPassword();
				if(this.comparePasswords(oldPasswordInputed, oldPasswordStored)) {
					if(newPassword.equals(newPasswordConfirmation)) {
						if(newPassword.length() >= 3) {
							String newPasswordHashWithSalt = this.generateHashWithSalt(newPassword);
							Database database = new Database();
							database.update("users", ((User) request.getSession().getAttribute("user")).getId(), "password", newPasswordHashWithSalt);
							database.saveFile();
							request.getSession().invalidate();
							request.setAttribute("result", "Password was successfully changed ! Please log in again.");
						}else {
							throw new Exception("New password is too short !");
						}
					}else {
						throw new Exception("New passwords do not match !");
					}
				}else {
					throw new Exception("Old passwords do not match !");
				}
				
			}else {
				throw new Exception("Please log in !");
			}
		}catch(Exception e) {
			request.setAttribute("result", e.getMessage());
		}
	}
	

	public void deleteUser(HttpServletRequest request, HttpServletResponse response) {
		try {
			if(this.isSessionAuthenticated(request, response)) {
				Database database = new Database();
				database.delete("users", ((User) request.getSession().getAttribute("user")).getId());
				database.saveFile();
				request.getSession().invalidate();
				request.setAttribute("result", "User was successfully deleted !");
			}else {
				throw new Exception("Please log in !");
			}
		}catch(Exception e) {
			request.setAttribute("result", e.getMessage());
		}
		
	}
	
	/////////////////////

	public void valdiateDataForRegistration(String username, String password, String passwordConfirmation) throws Exception {
		if(username.trim().length() < 3) {
			throw new Exception("Username must have at least 3 characters !");
		}else if(password.trim().length() < 3){
			throw new Exception("Password must have at least 3 characters !");
		}else if(!password.equals(passwordConfirmation)) {
			throw new Exception("Passwords do not match !");
		}
	}
	
	public void insertNewUser(String username, String password, String passwordConfirmation) throws Exception {
		try {
			Hashtable<String, String> data = new Hashtable<String, String>();
		
			data.put("username", username);
			data.put("password", this.generateHashWithSalt(password));
			data.put("is_locker", "false");
			data.put("id", UUID.randomUUID().toString());
			Database database = new Database();
			database.insert(data, "users", "user");
		}catch(Exception e) {
			System.out.println("Registration error\n" + e);
			throw new Exception("Registration error");
		}
	}

	private String generateHashWithSalt(String password) throws NoSuchAlgorithmException {
		String hashedPassword = ""; 
		String salt = UUID.randomUUID().toString();
		
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update((password + salt).getBytes());
		byte[] digestedBytes = md.digest();
		hashedPassword = DatatypeConverter.printHexBinary(digestedBytes).toLowerCase();
		return hashedPassword + "." + salt;
	}
	
	private boolean comparePasswords(String inputedPassword, String storedPasswordHashWithSalt) throws Exception{
		
		String[] hashAndSalt = storedPasswordHashWithSalt.split("\\.");
		String storedHash = hashAndSalt[0];
		String storedSalt = hashAndSalt[1];
				
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update((inputedPassword + storedSalt).getBytes());
			byte[] inputedPasswordWithSaltByteDigest = md.digest();
			
			String inputedPasswordWithSaltHash = DatatypeConverter.printHexBinary(inputedPasswordWithSaltByteDigest).toLowerCase();
			
			return storedHash.equals(inputedPasswordWithSaltHash);
			
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("Login error");
		}
		
	}

	public void validateLogin(String username, String password) throws Exception {
		if(username.trim() == "") {
			throw new Exception("Please fill username !");
		}else if(password.trim() == "") {
			throw new Exception("Please fill password !");
		}
	}


}
