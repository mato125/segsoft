package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.sun.jmx.snmp.Timestamp;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class Database {
	
	private String DATABASEFILE = "data.xml";
	private Document document;
	private String filePath;
	private File xmlFile;
	
	private NodeList selectedTable;
	private Node selectedRow;
	private Hashtable<String, String> selectedColumns;
	private Node selectedColumn;
	
	public Database() {
		this.getDatabase();
	}
	
	public Database getDatabase() {
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			String path = classLoader.getResource(this.DATABASEFILE).getFile();

			File xmlFile = new File(path);
			System.out.println("path to xml:\n"+path);
			this.filePath = path;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setIgnoringComments(true);
			dbFactory.setIgnoringElementContentWhitespace(true);
			DocumentBuilder bulider = dbFactory.newDocumentBuilder();
			
			this.document =  bulider.parse(xmlFile);	
			this.xmlFile = xmlFile;
		}catch(Exception e) {
			System.out.println("Error while loading file.\n"+e);
			
		}
		return this;
	}
	
	public Database getTable(String name) {
		if(this.document != null) {
			this.selectedTable = this.document.getElementsByTagName(name);
		}else {
			System.out.println("no database");
		}
		
		return this;
	}
	
	public Database getRowById(String id) {
		if(this.selectedTable != null) {
			for(int i = 0; i < this.selectedTable.getLength(); i++) {
	
				Node node = this.selectedTable.item(i);
							
				NodeList nodeList = node.getChildNodes();
				
				for(int j = 0; j < nodeList.getLength(); j++) {
					Node itemNode = nodeList.item(j);
					if (itemNode instanceof Element) {
						Element itemElement = (Element) itemNode;
						if(itemElement.getAttribute("id").equals(id)) {
							this.selectedRow = itemNode;
							return this;
						}
					}
				}
			}
	
			this.selectedRow = null;
		}else {
			System.out.println("no table");
		}
		return this;
	}
	
	public Database where(String column, String value) {
		if(this.selectedTable != null) {
			for(int i = 0; i < this.selectedTable.getLength(); i++) {
	
				Node node = this.selectedTable.item(i);
				
				NodeList nodeList = node.getChildNodes();
				
				for(int j = 0; j < nodeList.getLength(); j++) {
					
					Node itemNode = nodeList.item(j);
						
					NodeList columnNodes = itemNode.getChildNodes();
					 
					for(int k = 0; k < columnNodes.getLength(); k++) {
						Node columnNode = columnNodes.item(k);
						if (columnNode instanceof Element) {
						    Element childElement = (Element) columnNode;
					    
						    if(childElement.getTagName().equals(column) && childElement.getTextContent().equals(value)) {
						    	this.selectedRow = itemNode;
								return this;
							}
						    
						}
						
					}
				}
			}
			
			this.selectedRow = null;
		}else {
			System.out.println("no table");
		}
		return this;
	}
	
	public Database getColumns() {
		if(this.selectedRow != null) {
			Hashtable<String, String> columns = new Hashtable<String, String>();
			NodeList columnNodes = this.selectedRow.getChildNodes();
		 
			for(int i = 0; i < columnNodes.getLength(); i++) {
				Node columnNode = columnNodes.item(i);
				columns.put(columnNode.getNodeName(),columnNode.getTextContent());
				
			}
			this.selectedColumns = columns;
		}else {
			System.out.println("no row");
		}
		return this;
	}
	
	public String getColumn(String name) {
		this.getColumns();
		if(this.selectedColumns != null) {
			return this.selectedColumns.get(name);
		}else {
			System.out.println("no columns");
			return null;
		}
		
	}
	
	public Hashtable<String, String> getAllColumns(){
		this.getColumns();
		return this.selectedColumns;
	}
	
	public String getValue() {
		if(this.selectedColumn != null) {
			return this.selectedColumn.getNodeValue();
		}else {
			System.out.println("NO column");
			return null;
		}
		
	}
	
	public boolean insert(Hashtable<String, String> data,  String table, String tagName) {
		try {
			Element mainElement = document.createElement(tagName);
			Set<String> keys = data.keySet();
		    Iterator<String> itr = keys.iterator();
		    String key;
		    while (itr.hasNext()) { 
		       key = itr.next();
		       Element itemElement = document.createElement(key);
		       Text itemElementValue = document.createTextNode(data.get(key));
		       itemElement.appendChild(itemElementValue);
		       mainElement.appendChild(itemElement);
		    } 
		    mainElement.setAttribute("id", data.get("id"));
		    document.getElementsByTagName(table).item(0).appendChild(mainElement);
		    this.saveFile();
		    
		    return true;
		    
		}catch(Exception e) {
			
			System.out.println(e);
			return false;
			
		}
	}
	
	public void update(String table, String rowId, String column, String value) {
		NodeList rowsNodes = this.document.getElementsByTagName(table).item(0).getChildNodes();
		for(int i = 0; i < rowsNodes.getLength(); i++) {
			Node rowNode = rowsNodes.item(i);
			if (rowNode instanceof Element) {
				Element rowElement = (Element) rowNode;
				if(rowElement.getAttribute("id").equals(rowId)) {
					rowElement.getElementsByTagName(column).item(0).setTextContent(value);
				}
			}
			
		}
	}
	
	public void delete(String table, String rowId) {
		NodeList rowsNodes = this.document.getElementsByTagName(table).item(0).getChildNodes();
		for(int i = 0; i < rowsNodes.getLength(); i++) {
			Node rowNode = rowsNodes.item(i);
			if (rowNode instanceof Element) {
				Element rowElement = (Element) rowNode;
				if(rowElement.getAttribute("id").equals(rowId)) {
					rowNode.getParentNode().removeChild(rowNode);
				}
			}
		}
	}
	
	public void saveFile() throws IOException {
		
		try {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(this.document);
		StreamResult result = new StreamResult(new File(this.filePath));
		transformer.transform(source, result);

        } catch (Exception e) {
            System.out.println(e);
        }
	}

	
}
