package dto;

import java.util.Hashtable;

public class User {
	
	private String name;
	private String id;
	private String password;
	
	public User(Hashtable<String, String> userData) {
		this.name = userData.get("username");
		this.id = userData.get("id");
		this.password = userData.get("password");
	}
	public String getName() {
		return name;
	}
	public void setUserName(String userName) {
		this.name = userName;
	}
	public String getId() {
		return id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setId(String userId) {
		this.id = userId;
	}
	
	
}
