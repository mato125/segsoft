<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="dto.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="navigation.jsp" />
<h1>Change password</h1>
<% 
	String result = (String) request.getAttribute("result");
	
	if(result != null){
		out.print("<h4>" + result + "</h4>");
	};

	User user = (User) session.getAttribute("user");
	if(user == null){
		out.print("<h4>Firstly you have to log in !</h4>");
	}else{
%>
	<form action="changePassword" method="post">
		<label for='oldPassword'>old password: </label><br>
		<input type='password' name='oldPassword' /></br>
		
		<label for='newPassword'>new password: </label><br>
		<input type='password' name='newPassword' /></br>
		
		<label for='newPasswordConfirmation'>password confirmation</label><br>
		<input type='password' name='newPasswordConfirmation' /></br>
		
		<input type='submit' value='Change' />
	</form>
<%
}
%>
</body>
</html>