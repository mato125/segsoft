<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="dto.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="navigation.jsp" />
<% 
	String result = (String) request.getAttribute("result");
	
	if(result != null){
		out.print("<h4>" + result + "</h4>");
	};

	User user = (User) session.getAttribute("user");
	if(user != null){

		out.print("<h4>Hello " + user.getName() + " !</h4>");
		out.print("<a href='logout'>Logout</a>");
	}else{
%>
	<form action="login" method="post">
		<h1>Login</h1>
		<label for='username'>username: </label><br>
		<input type='text' name='username' /></br>
		
		<label for='password'>password: </label><br>
		<input type='password' name='password' /></br>
		
		<input type='submit' value='login' />
	</form>
<%
}
%>
</body>
</html>