<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="dto.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="navigation.jsp" />
<%
User user = (User) session.getAttribute("user");
if(user != null){

	out.print("<h4>You are already signedup !</h4>");
	out.print("<a href='logout'>Logout</a>");
}else{
	String result = (String) request.getAttribute("result");
	if(result != null){
		out.print("<h4>" +result + "</h4>");
	}
%>

<form action="signup" method="post">
	<h1>Signup</h1>
	<label for='username'>username: </label><br>
	<input type='text' name='username' /></br>
	
	<label for='password'>password: </label><br>
	<input type='password' name='password' /></br>
	
	<label for='password'>password confirmation: </label><br>
	<input type='password' name='passwordConfirmation' /></br>
	
	<input type='submit' value='create account' />
</form>
<%
}
%>
</body>
</html>