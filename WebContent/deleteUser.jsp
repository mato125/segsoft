<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="dto.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="navigation.jsp" />
<h1>Delete user</h1>
<% 
	String result = (String) request.getAttribute("result");
	
	if(result != null){
		out.print("<h4>" + result + "</h4>");
	};

	User user = (User) session.getAttribute("user");
	if(user == null){
		out.print("<h4>Firstly you have to log in !</h4>");
	}else{
		out.print("<a href='deleteUser'>delete user</a>");
	}
%>
</body>
</html>